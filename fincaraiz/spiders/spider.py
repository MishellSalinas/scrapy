import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from fincaraiz.items import FincaraizItem
from scrapy.exceptions import CloseSpider

class fincaraizSpider(CrawlSpider):
    name = 'fincaraiz'
    item_count = 0
    allowed_domains = ['https://www.fincaraiz.com.co/']
    start_urls = ['https://www.fincaraiz.com.co/casa-en-venta/bogota/']

    rules = {
        Rule(LinkExtractor(allow = (), restrict_xpaths = ('//div[@class="pagination"]//a'))),
        Rule(LinkExtractor(allow = (), restrict_xpaths = ('//li[@class="title-grid"]')),
        callback = 'parse_item', follow = False),
    }

    def parse_item(self, response):
        fr_item = FincaraizItem()
        #fr_item['titulo'] = response.xpath('normalize-space(//*[@id="aspnetForm"]/div[6]/div[1]/div[1]/div[2]/div[2]/h1)').extract()
        fr_item['sector'] = response.xpath('normalize-space(//*[@id="aspnetForm"]/div[6]/div[1]/div[1]/div[2]/div/h1/span)').extract()
        fr_item['direccion'] = response.xpath('normalize-space(//*[@id="divAdressBuilder"]/span)').extract()
        fr_item['area_privada'] = response.xpath('normalize-space(//*[@id="AdvertDetail"]/div[2]/div[5]/ul/li[1]/text())').extract()
        fr_item['area_contruida'] = response.xpath('normalize-space(//*[@id="AdvertDetail"]/div[2]/div[5]/ul/li[2]/text())').extract()
        fr_item['estrato'] = response.xpath('normalize-space(//*[@id="AdvertDetail"]/div[2]/div[5]/ul/li[5]/text())').extract()
        fr_item['descripcion'] = response.xpath('normalize-space(//*[@id="AdvertDetail"]/div[2]/div[7]/div/p/text())').extract()
        fr_item['valor'] = response.xpath('normalize-space(//*[@id="aspnetForm"]/div[6]/div[1]/div[1]/div[3]/h2)').extract()
        fr_item['localizacion'] = response.xpath('//*[@id="aspnetForm"]/div[6]/div[1]/script[1]/text()').extract()
        self.item_count += 1
        if self.item_count > 20:
            raise CloseSpider('item_exceeded')
        yield fr_item
