# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class FincaraizItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    titulo = scrapy.Field()
    sector = scrapy.Field()
    direccion = scrapy.Field()
    area_privada = scrapy.Field()
    area_contruida = scrapy.Field()
    estrato = scrapy.Field()
    descripcion = scrapy.Field()
    valor = scrapy.Field()
    localizacion = scrapy.Field()
    pass
